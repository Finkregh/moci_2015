package eu.telecomnancy;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ProxySensor;
import eu.telecomnancy.sensor.SimpleSensorLogger;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args)
    {
    	AbstractSensor sensor = new ProxySensor(new TemperatureSensor(), new SimpleSensorLogger());
        new MainWindow(sensor);
    }

}
