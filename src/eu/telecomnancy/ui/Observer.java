package eu.telecomnancy.ui;

public interface Observer {
	public void refresh();
}
