package eu.telecomnancy.ui;
import java.util.ArrayList;
import java.util.List;

public abstract class Observable {
	
	private List<Observer> list_obs = new ArrayList<Observer>();
	
	public void addObservater(Observer observer)
	{
		list_obs.add(observer);
    }
	
	public void delObservater(Observer observer)
	{
		list_obs.remove(observer);
	}
	
	public void notifierObservater()
	{

      for (Observer obs: this.list_obs)
      {
    	  obs.refresh();
      }
	}
}
      
