package eu.telecomnancy.sensor;

public class StateOff extends AbstractState {

	public StateOff(StateTemperatureSensor sts) {
		super(sts);
	}

	@Override
	public void on()
	{
		this.sts.setState(new StateOn(this.sts));
	}
		
		
	@Override
	public void off()
	{
	}

	@Override
	public boolean getStatus()
	{
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException
	{
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException
	{
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}



}
