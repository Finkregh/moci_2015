package eu.telecomnancy.sensor;

public class LegacyTemperatureSensorToISensorAdaptator implements ISensor {

	private LegacyTemperatureSensor instance = new LegacyTemperatureSensor();
	private double temperature;
	
	public LegacyTemperatureSensorToISensorAdaptator(final LegacyTemperatureSensor instance)
	{
		this.instance=instance;		
	}
	
	
	@Override
	public void on() {
		if (!instance.getStatus())
			{
			instance.onOff();
			}
	}

	
	@Override
	public void off() {
		if (instance.getStatus())
			{
			instance.onOff();
			}

	}

	@Override
	public boolean getStatus() {
		return instance.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (instance.getStatus())
		{
			temperature = instance.getTemperature();
		}
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
			if (instance.getStatus())
			{
				return temperature;
			}
			else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
}
