package eu.telecomnancy.sensor;

public abstract class AbstractState extends AbstractSensor {
	protected StateTemperatureSensor sts;

	protected AbstractState (StateTemperatureSensor sts)
	{
		super();
		this.sts = sts;
	}
	
	@Override
	public abstract void on();
	
}
