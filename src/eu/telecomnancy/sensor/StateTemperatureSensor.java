package eu.telecomnancy.sensor;

public class StateTemperatureSensor extends AbstractSensor {

	double tmp = 0;
	
	public double getValeur() {
		return tmp;
	}

	public void setValeur(double tmp) {
		this.tmp = tmp;
	}

	protected AbstractState state = new StateOff(this);
	
	
	public void setState(AbstractState state) {
		this.state = state;
	}

	@Override
	public void on()
	{
		this.state.on();
		notifierObservater();
	}

	@Override
	public void off()
	{
		this.state.off();
		notifierObservater();
	}

	@Override
	public boolean getStatus()
	{
		return this.state.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException
	{
		this.state.update();
		notifierObservater();
	}

	@Override
	public double getValue() throws SensorNotActivatedException
	{
		return this.state.getValue();
	}

}
