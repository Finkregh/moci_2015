package eu.telecomnancy.sensor;

import java.util.Date;

import eu.telecomnancy.ui.Observer;

public class ProxySensor extends AbstractSensor {

	public static Date date;
	private AbstractSensor sensor;
	private SensorLogger log;
	
	public ProxySensor(AbstractSensor sensor, SensorLogger log)
	{
		this.sensor = sensor;
		this.log = log;
	}

	@Override
	public void on() {
		log.log(LogLevel.INFO, "Sensor On");
		sensor.on();
		date = new Date();
		System.out.println(date.toString() + " on " + sensor.getStatus() );
	}

	@Override
	public void off() {
		log.log(LogLevel.INFO, "Sensor Off");
		sensor.off();
		date = new Date();
		System.out.println(date.toString() +" off " + sensor.getStatus() );
		
	}

	@Override
	public boolean getStatus() {
		log.log(LogLevel.INFO, "Sensor getStatus");
		date = new Date();
		System.out.println(date.toString() +" status " + sensor.getStatus() );
		return sensor.getStatus();
		
	}

	@Override
	public void update() throws SensorNotActivatedException {
		log.log(LogLevel.INFO, "Sensor update");
		sensor.update();
		date = new Date();
		System.out.println(date.toString() +" update " + sensor.getValue() );
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		log.log(LogLevel.INFO, "Sensor value =" + sensor.getValue());
		date = new Date();
		System.out.println(date.toString() +" Value " + sensor.getValue() );
		return sensor.getValue();
	}
	
	public void addObservater(Observer obs){
		sensor.addObservater(obs);
	}


}
