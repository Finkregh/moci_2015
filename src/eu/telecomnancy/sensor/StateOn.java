package eu.telecomnancy.sensor;

public class StateOn extends AbstractState {

	public StateOn(StateTemperatureSensor sts) {
		super(sts);
	}

	@Override
	public void on()
	{
	}
		
		
	@Override
	public void off()
	{
		this.sts.setState(new StateOff(this.sts));
	}

	@Override
	public boolean getStatus()
	{
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException
	{
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException
	{
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}



}
